/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Артур
 */
import java.util.ArrayList;
import java.util.List;

public class Port {

    List <PortFacility> PF = new ArrayList <PortFacility> ();    //Все терминалы порта
    int emptyPlace;                                              //количество свободных мест на складе
    List <Client> CL = new ArrayList <Client> ();                //все клиенты порта

    public void Port (List <PortFacility> PF1, int e,  List <Client> CL1){
        this.PF = PF1;
        this.emptyPlace = e;
        this.CL = CL1;
    }

    public int getEmptyPlace() {
        return emptyPlace;
    }

    public List<Client> getCL() {
        return CL;
    }

    public List<PortFacility> getPF() {
        return PF;
    }
}

