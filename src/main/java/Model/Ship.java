/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Артур
 */
public class Ship {

    int goods_to;           //количество груза, который необходимо загрузить на корабль
    int goods_from;         //количество грузка, который должен быть выгружен с корабля
    int fuel;               //количество топлива, которое нужно заправить на судно
    int water;              //количество воды, которую нужно загрузить на судно
    int food;               //количетво еды, которую нужно загрузить на судно
    int priority;           //приоритетность груза м.б. 1-5, чем больше, тем выше
    String name_ship;       //наименование судна

    public void addShip (int gt, int gf, int f, int w, int fo, int p, String s){
        this.goods_to = gt;
        this.goods_from = gf;
        this.fuel = f;
        this.water = w;
        this.food = fo;
        this.priority = p;
        this.name_ship = s;
    }

    public int getWater() {
        return water;
    }

    public int getFuel() {
        return fuel;
    }

    public int getFood() {
        return food;
    }

    public int getGoods_from() {
        return goods_from;
    }

    public int getGoods_to() {
        return goods_to;
    }

    public int getPriority() {
        return priority;
    }

    public void setGoods_from(int goods_from) {
        this.goods_from = goods_from;
    }

    public void setGoods_to(int goods_to) {
        this.goods_to = goods_to;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getName_ship() {
        return name_ship;
    }
}

