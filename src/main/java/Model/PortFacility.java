/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Артур
 */
import java.util.ArrayList;
import java.util.List;

public class PortFacility {

    int railway;                                        //наличие жд транспорта. если 0, то нет, иначе сколько контейнеров может увезти за час
    int trucks;                                         //наличие авто транспорта. если 0, то нет, иначе сколько контейнеров может увезти за час
    int fuel;                                           //наличие топливозаправщика. если 0, то нет, скорость заправки в литрах в час
    int water;                                          //наличие водозаправщика. если 0, то нет, скорость заправки в литрах в час
    int food;                                           //наличие пищезаправщика. если 0, то нет, скорость заправки в тоннах в час
    List<PortCrane> PC = new ArrayList<PortCrane>();    //количество и качество кранов в терминале
    int time;                                           //удаленность терминала от склада (время перемещение груза на терминал
    String name_fac;                                    //наименование терминала


    public void AddPortCrane (int r, int t, int f, int w, int fo, List <PortCrane> PC1, int time, String s){
        for (int i = 0; i < PC1.size(); i++){
            PC.add(PC1.get(i));
        }
        this.railway = r;
        this.trucks = t;
        this.fuel = f;
        this.water = w;
        this.food = fo;
        this.time = time;
        this.name_fac = s;
    }
    
    public void setRailway (int railway1){
        this.railway = railway1;
    }

    public void setTrucks (int trucks1){
        this.trucks = trucks1;
    }
    
    public void setFuel (int fuel1){
        this.fuel = fuel1;
    }
    
    public void setWater (int water1){
        this.water = water1;
    }
    
    public void setFood (int food1){
        this.food = food1;
    }
    
    public String getName_fac() {
        return name_fac;
    }

    public List<PortCrane> getPC() {
        return PC;
    }

    public int getTime() {
        return time;
    }

    public int getFood() {
        return food;
    }

    public int getFuel() {
        return fuel;
    }

    public int getRailway() {
        return railway;
    }

    public int getTrucks() {
        return trucks;
    }

    public int getWater() {
        return water;
    }
}
