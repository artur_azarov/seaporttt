/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Артур
 */
public class Client {

    int goods_to;           //количество груза от клиента на отправку
    int goods_from;         //количетсво груза от клиента на доставку
    int goods_readytoget;   //количество груза, который клиент готов забрать сразу с разгрузки
    String name;        //наименование клиента

    public void setClient (int wt, int wf, int wr, String s){
        this.goods_to = wt;
        this.goods_from = wf;
        this.goods_readytoget = wr;
        this.name = s;
    }

    public int getGoods_from() {
        return goods_from;
    }

    public int getGoods_readytoget() {
        return goods_readytoget;
    }

    public int getGoods_to() {
        return goods_to;
    }

    public String getClientName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGoods_from(int goods_from) {
        this.goods_from = goods_from;
    }

    public void setGoods_readytoget(int goods_readytoget) {
        this.goods_readytoget = goods_readytoget;
    }

    public void setGoods_to(int goods_to) {
        this.goods_to = goods_to;
    }
}

