/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Артур
 */
public class ShipTime {
    int time;               //общее время обработки судна
    String name_ship;       //наименование судна
    
    public void AddShipTime (int time, String name_ship){
        this.time = time;
        this.name_ship = name_ship;
    }
    
    public void setTime (int time){
        this.time = time;
    }

    public void setNameS (String name_ship){
        this.name_ship = name_ship;
    }
    
    public int getTime (){
        return time;
    }
    
    public String getName (){
        return name_ship;
    }

}
